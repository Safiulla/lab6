#include <iostream>
using namespace std;

void print_array(int sample[], int length) {
  std::cout << "[";
  for (int i=0; i<(length-1); i++){
    cout << sample[i] << ", ";
  }
  cout << sample[(length-1)] << "]" << endl;
}

int main() {
  int sample[] = {1, 2, 3};
  print_array(sample, 3);
}
