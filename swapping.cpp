#include <iostream>
using namespace std;

void swap_array(int sample[], int num1, int num2) {
  int inter;
  inter = sample[num1];
  sample[num1] = sample[num2];
  sample[num2] = inter;
}

void print_array(int sample[], int length) {
  std::cout << "[";
  for (int i=0; i<(length-1); i++){
    cout << sample[i] << ", ";
  }
  cout << sample[(length-1)] << "]" << endl;
}

int main() {
  int sample[] = {1, 2, 3};

  print_array(sample, 3);
  swap_array(sample, 0, 1);
  print_array(sample, 3);
}
