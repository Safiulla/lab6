#include <iostream>
using namespace std;

int main(int argc, char *argv[]) {
  std::cout << "Program name: " << "\'" << argv[0] << "\'" << endl ;
  std::cout << "Called with " << argc-1 << " arguments: ";
  for(int i=1; i < argc; i++){
    std::cout << "\'" << argv[i] << "\' ";
  }
}
