#include <iostream>
#include <string>
#include <fstream>

using namespace std;

int main (int argc, char *argv[]) {
    ifstream infile(argv[1]);
    ofstream outfile(argv[2]);
    string content = "";

    for(int i=0 ; infile.eof()!=true ; i++)
      content += infile.get();

    content.erase(content.end()-1);
    infile.close();

    outfile << content;
    outfile.close();
    return 0;
}
