#include <iostream>
#include <math.h>
using namespace std;

int cubed(int val) {
  return pow(val, 3); // or val * val * val if you want to be stingy
}

int main() {
  int val;
  std::cout << "Enter a number: ";
  std::cin >> val;
  std::cout << "Its cube is: " << cubed(val);
}
